# About Wok

**Wok** helps to organize multiple git repositories into a single multi-project
workspace.

**Wok** is a command line tool intended to be used alongside `git` in cases that require
multiple git repositories to be managed in connection with each other.

**Wok** utilizes `git submodules` to control several repositories and therefor requires
a separate parent (*umbrella*) repository.


## Learn about Wok

* [Why Wok?](about/why-wok.md) — about motivation behind creating `Wok`
* [Wok Overview](about/overview.md) — a quick overview of `Wok` mechanics
* [Quickstart](about/quickstart.md) — start to use `Wok`
* [CLI Reference](reference.md) — `Wok` command line interface reference


## Install Wok

### Pre-built binaries

> 🚧 TBD

### Install using `cargo`

> 🚧 TBD

### Build from source

1. Install Rust

See [https://www.rust-lang.org/learn/get-started](https://www.rust-lang.org/learn/get-started).

2. Clone Wok repository

```shell
git clone https://gitlab.com/wokdev/wok && cd ./wok
```

3. Build Wok

```shell
cargo build --release
```

4. Copy Wok executable to the location of your choice

```shell
cp ./target/release/wok /path/to/install/location
```


## Contribute to Wok

Contributions in any form and in any amount from anyone are always welcome.

`Wok` is licensed under [MIT License](https://gitlab.com/wokdev/wok/-/blob/main/LICENSE).

Please, read our [contributing guide](contributing.md) and [Code of Conduct](https://gitlab.com/wokdev/wok/-/blob/main/CODE_OF_CONDUCT.md).

## Resources

* [wok.dev](https://wok.dev/) — Wok docs (this site)
* [Wok on Gitlab](https://gitlab.com/wokdev/wok) — source code, pull requests, releases, issues, suggestions, getting help
* [Wok on Github](https://github.com/wokdev/wok/) — source code, pull requests
* [Code of Conduct](https://gitlab.com/wokdev/wok/-/blob/main/CODE_OF_CONDUCT.md) — Contributor Covenant Code of Conduct (adapted from the [Contributor Covenant](https://www.contributor-covenant.org/))
